﻿namespace DownloadScheduler
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Xml;

    using log4net;

    internal class ScheduleReader
    {
        private static ILog logger = null;

        private ArrayList _ScheduleOutPaths = new ArrayList();

        /// <summary>
        /// The main / constructor for this class
        /// This class shall read the XML-config/jobs file
        /// </summary>
        /// 
        public ScheduleReader()
        {
            ThreadContext.Properties["JOBNAME"] = "Download Scheduler";
            log4net.Config.XmlConfigurator.Configure();
            logger = LogManager.GetLogger(typeof(ScheduleReader));
        }

        private string _LogMessage { get; set; }

        private string _ScheduleJobname { get; set; }

        private string _SchedulePath { get; set; }

        private string _ScheduleFilename { get; set; }

        private bool _ScheduleUnzip { get; set; }

        public string _ScheduleExtention { get; set; }

        public string _ScheduleZipTarget { get; set; }

        public void setSchedules()
        {
            try
            {
                // Resets values
                this._SchedulePath = "";
                this._ScheduleJobname = "";

                var arrSchedules = new List<Schedules>();

                // Get the time of day Only hours and minutes
                // This code is used so that we can get the current job(s) being done at this time
                var hour = DateTime.Now.Hour.ToString().PadLeft(2, '0');
                var minutes = DateTime.Now.Minute.ToString().PadLeft(2, '0');
                ;

                // Creating an xPath strings
                var doc = new XmlDocument();

                var configFile = ConfigurationManager.AppSettings["ConfigurationSet"];

                logger.Debug("Loading: " + configFile);

                doc.Load(configFile);

                XmlNode node = null;
                try
                {
                    logger.Debug("Getting Jobname");
                    node =
                        doc.SelectSingleNode(
                            @"//job[child::input/schedule[@hour='" + hour + "'][@minute = '" + minutes
                            + "'][ancestor::job[@enable='true']]]/@name");

                    if (node != null)
                    {
                        this._ScheduleJobname = node.InnerText; // node.Attributes.GetNamedItem("name").Value;
                    }
                    logger.Debug("Done Getting Jobname: " + this._ScheduleJobname);
                }
                catch (Exception exception)
                {
                    logger.Error(exception.Message);
                    logger.Error(exception.StackTrace.ToString());
                }
                arrSchedules.Clear();

                try
                {
                    logger.Debug("Getting Path");
                    node =
                        doc.SelectSingleNode(
                            @"//job/input[child::schedule[@hour='" + hour + "'][@minute = '" + minutes
                            + "']][ancestor-or-self::job[@enable='true']][ancestor-or-self::job[@name='"
                            + this._ScheduleJobname + "']]/@path");

                    if (node != null)
                    {
                        this._SchedulePath = node.InnerText; // node.Attributes.GetNamedItem("path").Value;
                    }
                    logger.Debug("Done Getting Path: " + this._SchedulePath);
                }
                catch (Exception exception)
                {
                    logger.Error(exception.Message);
                    logger.Error(exception.StackTrace.ToString());
                }

                try
                {
                    logger.Debug("Getting unzip-status");
                    node =
                        doc.SelectSingleNode(
                            @"//job[child::input/schedule[@hour='" + hour + "'][@minute = '" + minutes
                            + "'][ancestor::job[@enable='true']]]/@unzip");

                    if (node != null)
                    {
                        this._ScheduleUnzip = Convert.ToBoolean(node.InnerText);
                            // Convert.ToBoolean(node.Attributes.GetNamedItem("unzip").Value);
                    }

                    logger.Debug("Done getting unzip-status: " + this._ScheduleUnzip.ToString());
                }
                catch (Exception exception)
                {
                    logger.Error(exception.Message);
                    logger.Error(exception.StackTrace.ToString());
                }

                // making sure that we have an extention - and XML is the default one
                this._ScheduleExtention = "xml";
                try
                {
                    logger.Debug("Getting extention");
                    node =
                        doc.SelectSingleNode(
                            @"//job[child::input/schedule[@hour='" + hour + "'][@minute = '" + minutes
                            + "'][ancestor::job[@enable='true']]]/@extention");

                    if (node != null)
                    {
                        this._ScheduleExtention = node.InnerText;
                            // Convert.ToBoolean(node.Attributes.GetNamedItem("unzip").Value);
                    }

                    logger.Debug("Done Getting extention: " + this._ScheduleExtention);
                }
                catch (Exception exception)
                {
                    logger.Error(exception.Message);
                    logger.Error(exception.StackTrace.ToString());
                }

                // This will get the output-paths based on the jobname
                try
                {
                    logger.Debug("Getting outpath(s)");
                    XmlNodeList nodeList =
                        doc.SelectNodes(
                            @"//job[child::input/schedule[@hour='" + hour + "'][@minute = '" + minutes
                            + "']][@enable='true'][@name='" + this._ScheduleJobname + "']/output/path");

                    if (nodeList != null)
                    {
                        foreach (XmlNode nd in nodeList)
                        {
                            if (nd != null)
                            {
                                this._ScheduleOutPaths.Add(nd.InnerText);
                                logger.Debug("Getting outpath: " + nd.InnerText);
                            }
                        }
                    }
                    logger.Debug("Done Getting outpath(s)");
                }
                catch (Exception exception)
                {
                    logger.Error(exception.Message);
                    logger.Error(exception.StackTrace.ToString());
                }

                // This will get the filename for the job
                try
                {
                    logger.Debug("Getting filename");
                    node =
                        doc.SelectSingleNode(
                            @"//job[child::input/schedule[@hour='" + hour + "'][@minute = '" + minutes
                            + "']][@enable='true'][@name='" + this._ScheduleJobname + "']/output/@filename");

                    if (node != null)
                    {
                        this._ScheduleFilename = node.InnerText; // node.Attributes.GetNamedItem("filename").Value;
                    }

                    logger.Debug("Done Getting filename: " + this._ScheduleFilename);
                }
                catch (Exception exception)
                {
                    logger.Error(exception.Message);
                    logger.Error(exception.StackTrace.ToString());
                }

                try
                {
                    logger.Debug("Getting ziptarget-path");
                    node =
                        doc.SelectSingleNode(
                            @"//job[child::input/schedule[@hour='" + hour + "'][@minute = '" + minutes
                            + "']][@enable='true'][@name='" + this._ScheduleJobname + "']/ziptarget/@path");

                    if (node != null)
                    {
                        this._ScheduleZipTarget = node.InnerText; // node.Attributes.GetNamedItem("filename").Value;
                    }
                    logger.Debug("Getting ziptarget-path: " + this._ScheduleZipTarget);
                }
                catch (Exception exception)
                {
                    logger.Error(exception.Message);
                    logger.Error(exception.StackTrace.ToString());
                }

                ArrayList SchedulePaths = new ArrayList();

                foreach (string paths in this._ScheduleOutPaths)
                {
                    SchedulePaths.Add(paths.ToString());
                }

                // Creating the Schedules object
                Schedules s = new Schedules();
                //s.Filename = _ScheduleFilename;
                s.Name = this._ScheduleJobname;
                s.Path = this._SchedulePath;
                s.Unzip = Convert.ToBoolean(this._ScheduleUnzip);
                s.Paths(SchedulePaths);
                s.Extention = this._ScheduleExtention;
                s.ZipTarget = this._ScheduleZipTarget;
                s.Filename = this._ScheduleFilename;

                // Clearing the settings
                this._ScheduleOutPaths.Clear();

                arrSchedules.Add(s);

                // Looping through schedules and apply them to the Downloader-class as an object
                if (arrSchedules.Count > 0)
                {
                    // Creating a new downloader object
                    int i = arrSchedules.Count;

                    foreach (Schedules t in arrSchedules)
                    {
                        if (t.Filename != null)
                        {
                            Downloader d = new Downloader();

                            d.DownloadJobname = t.Name;
                            d.DownloadPath = t.Path;
                            d.DownloadFilename = t.Filename;
                            d.DownloadExtention = t.Extention;
                            d.doUnzip = t.Unzip;
                            d.DownloadZipTarget = t.ZipTarget;

                            foreach (KeyValuePair<string, ArrayList> kvp in t.SchOutputPaths)
                            {
                                if (kvp.Key.ToString() == t.Name)
                                {
                                    foreach (string OutputPath in kvp.Value)
                                    {
                                        d.OutputPaths.Add(OutputPath);
                                    }
                                }
                            }
                            // This is a downloader..
                            d.doDownload();

                            // Destroy the download stuff
                            d.Destroy();
                        }
                    }
                    arrSchedules.Clear();
                }
                else
                {
                    this._LogMessage = "No download jobs";
                    logger.Debug(this._LogMessage);
                }
            }

            catch (Exception e)
            {
                this._LogMessage = e.Message.ToString();
                logger.Info(this._LogMessage);

                this._LogMessage = e.StackTrace.ToString();
                logger.Info(this._LogMessage);
            }
        }
    }
}