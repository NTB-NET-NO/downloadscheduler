﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DebugForm.cs" company="NTB">
//   NTB
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DownloadScheduler
{
    using System;
    using System.Windows.Forms;

    using global::DownloadScheduler.Components;

    using log4net;

    public partial class DebugForm : Form
    {
        /// <summary>
        /// Static _logger
        /// </summary>
        private static ILog logger = LogManager.GetLogger(typeof(DebugForm));

        /// <summary>
        /// The actual service object
        /// </summary>
        /// <remarks>Does the actuall work</remarks>
        protected MainServiceComponent Service;

        public DebugForm()
        {
            ThreadContext.Properties["JOBNAME"] = "Download Scheduler";
            log4net.Config.XmlConfigurator.Configure();
            logger = LogManager.GetLogger(typeof(DebugForm));

            this.Service = new MainServiceComponent();

            this.InitializeComponent();
        }

        private string LogMessage { get; set; }

        private void TimerSchedulerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            // Stopping the timer so that we don't have multiple jobs running at the same time
            this.timerScheduler.Stop();
            this.timerScheduler.Enabled = false;

            // And configure
            try
            {
                this.Service.Configure();
                this.Service.Start();
                logger.Info("Download Scheduler DEBUGMODE started");
            }
            catch (Exception ex)
            {
                logger.Fatal("Download Scheduler DEBUGMODE DID NOT START properly", ex);
            }
        }

        private void DebugFormLoad(object sender, EventArgs e)
        {
            try
            {
                this.LogMessage = "---------------------------------";
                logger.Info(this.LogMessage);
                this.LogMessage = "-- Starting Download Scheduler --";
                logger.Info(this.LogMessage);
                this.LogMessage = "---------------------------------";
                logger.Info(this.LogMessage);

                this.timerScheduler.Start();
            }
            catch (Exception ex)
            {
                logger.Fatal("SportsDataService DEBUGMODE DID NOT START properly", ex);
            }
        }
    }
}