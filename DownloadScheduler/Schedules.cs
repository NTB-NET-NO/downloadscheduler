﻿namespace DownloadScheduler
{
    using System.Collections;
    using System.Collections.Generic;

    public class Schedules
    {
        /// <summary>
        /// A dictionary telling which folders to store the jobs
        /// </summary>
        public Dictionary<string, ArrayList> SchOutputPaths = new Dictionary<string, ArrayList>();

        /// <summary>
        /// Name of the Schedule Job
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Input Path for the job - where to get the file / data
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Filename that is to be created
        /// </summary>
        public string Filename { get; set; }

        /// <summary>
        /// If we are to unzip the downloaded file
        /// </summary>
        public bool Unzip { get; set; }

        /// <summary>
        /// The extention the downloaded file shall have
        /// </summary>
        public string Extention { get; set; }

        public string ZipTarget { get; set; }

        public void Paths(ArrayList paths)
        {
            this.SchOutputPaths.Add(this.Name, paths);
        }
    }
}