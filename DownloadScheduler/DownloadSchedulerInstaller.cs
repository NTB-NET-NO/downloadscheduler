﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;


namespace DownloadScheduler
{
    [RunInstaller(true)]
    public partial class DownloadSchedulerInstaller : Installer
    {
        public DownloadSchedulerInstaller()
        {
            InitializeComponent();
        }
    }
}
