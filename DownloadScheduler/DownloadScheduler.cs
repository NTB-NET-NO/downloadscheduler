﻿namespace DownloadScheduler
{
    using System;
    using System.Configuration;
    using System.ServiceProcess;
    using System.Xml;

    using log4net;

    public partial class DownloadScheduler : ServiceBase
    {
        // Logger support
        private static ILog logger = null;

        // String used in exceptions
        private string _LogMessage = "";

        // Getting variables from the Config-file. 
        private readonly string pathError = ConfigurationManager.AppSettings["ErrorPath"];

        private readonly string pathJobfile = ConfigurationManager.AppSettings["ConfigFilePath"];

        // Creating a Scheduler Object
        private readonly Scheduler myScheduler = new Scheduler();

        // Creating an XML-document
        private XmlDocument xmlJobs = new XmlDocument();

        public DownloadScheduler()
        {
            ThreadContext.Properties["JOBNAME"] = "Download Scheduler";
            log4net.Config.XmlConfigurator.Configure();
            logger = LogManager.GetLogger(typeof(DownloadScheduler));

            this.InitializeComponent();
        }

        /// <summary>
        /// This method is called when the service starts
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            this._LogMessage = "---------------------------------";
            logger.Info(this._LogMessage);
            this._LogMessage = "-- Starting Download Scheduler --";
            logger.Info(this._LogMessage);
            this._LogMessage = "---------------------------------";
            logger.Info(this._LogMessage);

            this.timerScheduler.Interval = Convert.ToInt32(ConfigurationManager.AppSettings["Interval"]);
            this.timerScheduler.Enabled = true;
            this.myScheduler.PathError = this.pathError;
            this.myScheduler.PathJobFile = this.pathJobfile;
            this.myScheduler.Init();
            this.myScheduler.Run();
        }

        /// <summary>
        /// This method is called when the sevice ends
        /// </summary>
        protected override void OnStop()
        {
            this.timerScheduler.Enabled = false;
            this._LogMessage = "---------------------------------";
            logger.Info(this._LogMessage);
            this._LogMessage = "-- Stopping Download Scheduler --";
            logger.Info(this._LogMessage);
            this._LogMessage = "---------------------------------";
            logger.Info(this._LogMessage);
        }

        /// <summary>
        /// This method is called when the timer has elapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerScheduler_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            // Stopping the timer so that we don't have multiple jobs running at the same time
            this.timerScheduler.Stop();

            // Run the scheduler
            this.myScheduler.Run();

            // Starting the timer again
            this.timerScheduler.Start();
        }
    }
}