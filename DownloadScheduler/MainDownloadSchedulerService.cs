﻿using System;
using System.ServiceProcess;

// Adding reference to components
using DownloadScheduler.Components;


// Adding Log4Net - logging support
using log4net;
namespace DownloadScheduler
{
    public partial class MainDownloadSchedulerService : ServiceBase
    {
        static ILog _logger = LogManager.GetLogger(typeof(MainDownloadSchedulerService));

        protected MainServiceComponent Service = null;


        public MainDownloadSchedulerService()
        {
            //log4net.Config.XmlConfigurator.Configure();
            //MDC.Set("JOBNAME", "NTB.SportsData.Service");

            ThreadContext.Properties["JOBNAME"] = "Download Scheduler Service";
            log4net.Config.XmlConfigurator.Configure();

            _logger = LogManager.GetLogger(typeof(MainDownloadSchedulerService));

            _logger.Info("In MainNTBSportsDataService - starting up");
            // Creating the new MainServiceComponent
            Service = new MainServiceComponent();

            // Initialising this main component
            InitializeComponent();
        }

        private void _startupConfigTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _startupConfigTimer.Stop();

            try
            {
                Service.Configure();
                Service.Start();
                _logger.Info("MainDownloadSchedulerService Started!");
            }
            catch (Exception ex)
            {
                _logger.Fatal("MainDownloadSchedulerService DID NOT START properly - TERMINATING!"
                    , ex);

                throw;
            }
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                _logger.Info("NTB SportsDataService starting...");
                _startupConfigTimer.Start();
            }
            catch (Exception ex)
            {
                _logger.Fatal("NTBSportsDataService DID NOT START properly - Terminating!", ex);
                throw;
            }


        }

        protected override void OnStop()
        {
            try
            {
                _logger.Info("Stopping service");
                Service.Stop();


                _startupConfigTimer.Enabled = false;
            }
            catch (Exception ex)
            {
                _logger.Fatal("NTB SporstDataService did not stop properly - Terminating!"
                    , ex);
                throw;
            }
        }

    }
}
