﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using log4net;
using log4net.Config;
using System.IO;

namespace DownloadScheduler
{
    public class Scheduler
    {
        // log4net
        static ILog logger = null;

        public string PathError { get; set; }

        public string PathJobFile { get; set; }

        public string LogMessage { get; set; }


        public Scheduler()
        {
            log4net.ThreadContext.Properties["JOBNAME"] = "Downloader";
            log4net.Config.XmlConfigurator.Configure();
            logger = LogManager.GetLogger(typeof(Scheduler));

        }
        public void Init()
        {
            LogMessage = "Initiating service";
            logger.Info(LogMessage);
            // Creating the Log-directory
            try
            {

                // We are checking if the directories exists
                this._CheckDirectories(this.PathError);
                this._CheckDirectories(this.PathJobFile);


            }
            
            catch (IOException ioe)
            {
                LogMessage = ioe.Message.ToString();
                logger.Info(LogMessage);

                LogMessage = ioe.StackTrace.ToString();
                logger.Info(LogMessage);
            }

            catch (Exception e)
            {
                LogMessage = e.Message.ToString();
                logger.Info(LogMessage);

                LogMessage = e.StackTrace.ToString();
                logger.Info(LogMessage);
            }

            
        }

        public void Run()
        {
            this.LogMessage = "Finding scheduled download jobs";
            logger.Debug(this.LogMessage);
            
            var myScheduleReader = new ScheduleReader();
            myScheduleReader.setSchedules();
                
        }

        

        private void _CheckDirectories(string Directory)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(Directory);
                if (!di.Exists)
                {
                    di.Create();
                }
            }
            catch (IOException ioe)
            {
                // We shall log the exception here
                LogMessage = ioe.Message.ToString();
                logger.Info(LogMessage);

                // Get the stack-trace too
                LogMessage = ioe.StackTrace.ToString();
                logger.Info(LogMessage);
            }

            catch (Exception e)
            {
                // We shall log the exception here
                LogMessage = e.Message.ToString();
                logger.Info(LogMessage);

                // Get the stack-trace too
                LogMessage = e.StackTrace.ToString();
                logger.Info(LogMessage);
            }
        }

        
    }
}