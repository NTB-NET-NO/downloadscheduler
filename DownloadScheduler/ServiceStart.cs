﻿using System.ServiceProcess;

// Adding support for log4Net logging
using log4net;
using log4net.Config;

namespace DownloadScheduler
{
    static class ServiceStart
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(MainDownloadSchedulerService));
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {

            XmlConfigurator.Configure();
            MDC.Set("JOBNAME", "Download Scheduler");
            Logger.Info("---------------------------------------------------------");
            Logger.Info("---       Starting up the NTB SportsData Service      ---");
            Logger.Info("---------------------------------------------------------");

            ServiceBase[] servicesToRun = new ServiceBase[] 
                { 
                    new MainDownloadSchedulerService() 
                };
            ServiceBase.Run(servicesToRun);
        }
    }
}
