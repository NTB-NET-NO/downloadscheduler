﻿using System;
using System.Text;
using System.IO;
using System.Net;
using System.Collections;
using System.Configuration;

using log4net;
using log4net.Config;

using Ionic;
using Ionic.Zip;

namespace DownloadScheduler
{
    class Downloader
    {
        // Getting variables from the Config-file. 
        
        // String used in exceptions
        private string _LogMessage { get; set; }

        // log4net
        static ILog logger = null;

        // A set of getters and setters

        /// <summary>
        /// Gets and sets the OutputFolder
        /// </summary>
        public string OutputFolder { get; set; }



        /// <summary>
        /// Gets of sets the DownloadJobname
        /// </summary>
        public string DownloadJobname { get; set; }


        /// <summary>
        /// Gets or sets the DownloadPath
        /// </summary>
        public string DownloadPath { get; set; }

        /// <summary>
        /// Gets or sets the DoUnzip
        /// </summary>
        public bool doUnzip { get; set; }

        /// <summary>
        /// Gets or adds values to the ArrayList alOutputPaths
        /// </summary>
        public ArrayList OutputPaths = new ArrayList();


        /// <summary>
        /// This string holds the filename related to the job
        /// </summary>
        public string DownloadFilename = "";


        public Downloader()
        {
            log4net.ThreadContext.Properties["JOBNAME"] = "Downloader";
            log4net.Config.XmlConfigurator.Configure();
            logger = LogManager.GetLogger(typeof(Downloader));


            // This one resets the values
            DownloadPath = "";
            DownloadJobname = "";
            DownloadFilename = "";
            doUnzip = false;
            OutputPaths.Clear();


        }

        public void Destroy()
        {
            // This one resets the values
            DownloadPath = "";
            DownloadJobname = "";
            DownloadFilename = "";
            doUnzip = false;
            OutputPaths.Clear();

        }

        public Downloader(string strJobname)
        {
            log4net.ThreadContext.Properties["JOBNAME"] = "Downloader";
            log4net.Config.XmlConfigurator.Configure();
            logger = LogManager.GetLogger(typeof(Downloader));

            this.DownloadJobname = strJobname;
        }

        public Downloader(string strJobname, string strDownloadPath)
        {
            log4net.ThreadContext.Properties["JOBNAME"] = "Downloader";
            log4net.Config.XmlConfigurator.Configure();
            logger = LogManager.GetLogger(typeof(Downloader));

            this.DownloadJobname = strJobname;
            this.DownloadPath = strDownloadPath;
        }

        public Downloader(string strJobname, string strDownloadPath, ArrayList arrFolders)
        {
            this.DownloadJobname = strJobname;
            this.DownloadPath = strDownloadPath;
            foreach (string folders in arrFolders)
            {
                OutputPaths.Add(folders);
            }


        }
        public void doDownload()
        {
            try
            {
                // Set some strings
                string strURL = this.DownloadPath;

                // Creating a unique filename
                DateTime dt = DateTime.Now;

                string year = dt.Year.ToString().PadLeft(2, '0');
                string month = dt.Month.ToString().PadLeft(2, '0');
                string day = dt.Day.ToString().PadLeft(2, '0');

                string strDt = year + "-" + month + "-" + day;

                // Putting the filename together
                string hour = dt.Hour.ToString().PadLeft(2, '0');
                string minutes = dt.Minute.ToString().PadLeft(2, '0');
                string seconds = dt.Second.ToString().PadLeft(2, '0');

                strDt += "_" + hour.ToString() + minutes.ToString() + seconds.ToString();

                // Putting the filename together
                string Filename = "";

                Filename = strDt + "_" + this.DownloadFilename;

                if (!Filename.Contains(this.DownloadExtention))
                {
                    Filename += "." + this.DownloadExtention;
                    Filename.Replace("..", ".");
                }

                // What if this is a binary file (like a zip-file)

                if (DownloadExtention == "xml")
                {
                    // Creating a stringbuilder
                    StringBuilder sb = new StringBuilder();

                    // used on each read operation
                    byte[] buf = new byte[8192];

                    // Prepare the web page request
                    try
                    {
                        logger.Debug("Trying to get in touch with: " + strURL);
                        WebRequest request = WebRequest.Create(strURL);
                        WebResponse response = request.GetResponse();
                    

                    Stream resStream = response.GetResponseStream();

                    string tempString = null;
                    int count = 0;


                    do
                    {
                        // fill the buffer with data
                        count = resStream.Read(buf, 0, buf.Length);

                        // make sure we read some data
                        if (count != 0)
                        {
                            // Translate from bytes to iso-8859-1

                            tempString = Encoding.GetEncoding(28605).GetString(buf, 0, count);

                            sb.Append(tempString);
                        }
                    }
                    while (count > 0); // any more data to read?

                    // Now we have some text, and so we shall save it
                    // create a writer and open the file

                    // If we have an arry that is larger than zero (in other words, if we have an array)
                    // We shall run the for each loop
                    //strMessage = "alOutputPaths.Count: " + alOutputPaths.Count.ToString();
                    //LogFile.WriteLog(ref PathLog, ref strMessage);

                    if (OutputPaths.Count > 0)
                    {
                        foreach (string Path in OutputPaths)
                        {
                            string OutputFile = Path + @"\" + Filename;
                            // We have added another layer of array....
                            _LogMessage = "Writing to: " + OutputFile;
                            logger.Info(_LogMessage);

                            TextWriter tw = null;

                            tw = new StreamWriter(OutputFile, false, Encoding.GetEncoding(28605));

                            // write a line of text to the file
                            tw.WriteLine(sb.ToString());

                            // close the stream
                            tw.Close();
                        }
                    }


                    // Clean sb-string
                    sb.Length = 0;
                    }
                    catch (WebException we)
                    {
                        logger.Fatal(we.Message);
                        logger.Error(we.StackTrace.ToString());
                    }
                }
                else
                {
                    try
                    {
                        logger.Info("Trying to get in touch with: " + strURL);
                        WebRequest request = WebRequest.Create(strURL);
                        WebResponse response = request.GetResponse();
                        logger.Debug("Done contacting");

                        foreach (string OutputPath in OutputPaths)
                        {
                            // Do the binary stuff
                            string OutputFile = OutputPath + @"\" + Filename;
                            logger.Info("Output to: " + OutputFile);

                            Stream responseStream = response.GetResponseStream();
                            // StreamWriter writer = new StreamWriter(OutputFile);

                            FileStream writer = new FileStream(OutputFile, FileMode.Create);
                            // Copy the data from the responseStream to destination 1k at a time (feel free to increase buffer size)
                            byte[] buffer = new byte[4096];

                            for (int amountRead = responseStream.Read(buffer, 0, buffer.Length); amountRead > 0; amountRead = responseStream.Read(buffer, 0, buffer.Length))
                            {
                                writer.Write(buffer, 0, amountRead);
                            }


                            writer.Close();
                            logger.Debug("Done writing " + OutputFile);


                            if (this.doUnzip == true)
                            {
                                logger.Info("Unzipping: " + OutputFile);
                                using (ZipFile myZipFile = ZipFile.Read(OutputFile))
                                {
                                    if (this.DownloadZipTarget == "")
                                    {
                                        DownloadZipTarget = OutputPath;
                                    }
                                    foreach (ZipEntry entry in myZipFile)
                                    {
                                        try
                                        {
                                            
                                            entry.Extract(this.DownloadZipTarget, ExtractExistingFileAction.OverwriteSilently);
                                            logger.Debug("Unzipping: " + this.DownloadZipTarget + @"\" + entry.FileName);
                                        }
                                        catch (Exception exception)
                                        {
                                            logger.Error(exception.Message);
                                            logger.Error(exception.StackTrace.ToString());
                                        }
                                    }


                                }
                                logger.Debug("Done unzipping: " + OutputFile);
                                // removing the downloaded file after we've unzipped the content
                                try
                                {
                                    // We might have to wait until the file becomes ready, but we can check for that...
                                    logger.Info("Removing download file");
                                    FileInfo fi = new FileInfo(OutputFile);
                                    fi.Delete();
                                    logger.Debug("Done Removing download file");
                                }
                                catch (Exception ex)
                                {
                                    logger.Error(ex.Message);
                                    logger.Error(ex.StackTrace.ToString());
                                }
                            }
                        }
                        response.Close();
                    }
                    catch (WebException we)
                    {
                        logger.Fatal(we.Message);
                        logger.Error(we.StackTrace.ToString());
                    }
                    
                }




            }
            catch (HttpListenerException he)
            {
                _LogMessage = he.Message.ToString();
                logger.Info(_LogMessage);

                _LogMessage = he.StackTrace.ToString();
                logger.Info(_LogMessage);

            }

            catch (IOException ioe)
            {
                _LogMessage = ioe.Message.ToString();
                logger.Info(_LogMessage);

                _LogMessage = ioe.StackTrace.ToString();
                logger.Info(_LogMessage);
            }

            catch (Exception e)
            {
                _LogMessage = e.Message.ToString();
                logger.Info(_LogMessage);

                _LogMessage = e.StackTrace.ToString();
                logger.Info(_LogMessage);

            }
        }


        public string DownloadExtention { get; set; }

        public string DownloadZipTarget { get; set; }
    }
}