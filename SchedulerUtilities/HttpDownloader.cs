﻿namespace DownloadScheduler.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;

    using log4net;

    public class HttpDownloader
    {
        // TODO: We need some sort of logger to store which file and when we have downloaded it. If we see that the current file is newer, then we can download it again
        private static readonly ILog Logger = LogManager.GetLogger(typeof(HttpDownloader));

        public string Server { get; set; }

        public string Path { get; set; }

        public List<string> LocalPaths { get; set; }

        public string Filename { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        public string Filter { get; set; }

        /// <summary>
        /// Gets or sets the instance name.
        /// </summary>
        public string InstanceName { get; set; }

        //public HttpDownloader()
        //{

        //}

        private string CreateFileName()
        {
            var filename = this.Filename;
            var dt = DateTime.Now;

            var year = dt.Year.ToString().PadLeft(2, '0');
            var month = dt.Month.ToString().PadLeft(2, '0');
            var day = dt.Day.ToString().PadLeft(2, '0');

            // Putting the filename together
            var hour = dt.Hour.ToString().PadLeft(2, '0');
            var minutes = dt.Minute.ToString().PadLeft(2, '0');
            var seconds = dt.Second.ToString().PadLeft(2, '0');

            filename =
                filename.Replace("[%Y]", year)
                    .Replace("[%M]", month)
                    .Replace("[%D]", day)
                    .Replace("[%H]", hour)
                    .Replace("[%m]", minutes)
                    .Replace("[%S]", seconds);
            return Regex.Replace(filename, @"\[\%\[A-Za-z]\]", "");
        }

        public string SetBasicAuthHeader(String userName, String userPassword)
        {
            string authInfo = userName + ":" + userPassword;
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            return "Basic " + authInfo;
        }

        public void Download()
        {
            try
            {
                Logger.Info("Trying to get in touch with: " + this.Server);
                foreach (var localPath in this.LocalPaths)
                {
                    var filename = this.CreateFileName();

                    var request = (HttpWebRequest)WebRequest.Create(this.Server);

                    if (!string.IsNullOrEmpty(this.Username))
                    {
                        request.Headers["Authorization"] = this.SetBasicAuthHeader(this.Username, this.Password);
                    }

                    var downloaded = false;
                    
                    using (var response = (HttpWebResponse)request.GetResponse())
                    {
                        var currentFile = response.ResponseUri.LocalPath;
                        var parts = currentFile.Split('/');
                        currentFile = parts.Last();
                        var lastModified = response.LastModified;

                        // Now we have file age and filename of file
                        // Now we need to check this data somewhere
                        var fileDate = DateTime.Today.Year + DateTime.Today.Month.ToString().PadLeft(2, '0')
                                       + DateTime.Today.Day.ToString().PadLeft(2, '0');
                        //var fileXml = ConfigurationManager.AppSettings["DownloadedFilePath"] + fileDate + "_"
                        //              + this.InstanceName + ".xml";
                        var fileXml = ConfigurationManager.AppSettings["DownloadedFilePath"] + this.InstanceName + ".xml";
                        var doc = new XmlDocument();

                        var xmlFileExists = new FileInfo(fileXml);
                        if (xmlFileExists.Exists)
                        {
                            doc.Load(fileXml);

                            // singleNode = doc.SelectSingleNode("//file[.='" + currentFile + "']");

                            // todo: Just check for filename first. Then for timestamp. Then we can add version
                            var singleNode =
                                doc.SelectSingleNode(
                                    "//file[@timestamp='" + lastModified.ToString(CultureInfo.InvariantCulture) + "' and @filename='" + currentFile + "']");

                            // If we cannot find this node, it means that we have not downloaded it
                            if (singleNode != null)
                            {
                                downloaded = true;
                                Logger.Info("File " + currentFile + " is downloaded already.");
                            }
                        }

                        
                        if (downloaded)
                        {
                            response.Close();
                            continue;
                        }
                        Logger.Debug("Done contacting");
                        // Do the binary stuff
                        // string filename = this.CreateFileName();

                        var outputFile = localPath + @"\" + filename;

                        // Logging the output
                        Logger.Info("Output to: " + outputFile);

                        // Reading data from the stream
                        using (var reader = new StreamReader(response.GetResponseStream()))
                        {
                            File.AppendAllText(outputFile, reader.ReadToEnd());
                        }
                        Logger.Debug("Done writing " + outputFile);

                        response.Close();

                        this.StoreLastDownloadInformation(doc, lastModified, outputFile, fileXml, currentFile);
                        
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
        }

        private void StoreLastDownloadInformation(XmlDocument doc, DateTime lastModified, string outputFile, string fileXml, string currentFile)
        {
            /*
             * As of 06.07.2015 this file looks like this
             * <files>
             *      <file timestamp="02.07.2015 13:00:03">Langoddsen</file>
             * </files>
             * 
             * I like to change it to:
             * @timestamp (same as today)
             * @filename (name of the file we created)
             * 
             * Currently we are also creating one file each day. That is not necessary. One file for each job.
             * Cleanup: We could remove elements where the date is older than 30 days.
             */
            // Now we shall add this file to the datafile
            XmlNode root = doc.DocumentElement;

            var element = doc.CreateElement("file");
            element.SetAttribute("timestamp", lastModified.ToString(CultureInfo.InvariantCulture));
            element.SetAttribute("filename", currentFile);
            element.SetAttribute("downloadPath", outputFile);
            
            Logger.Info("Storing information about download in xml-file");
            if (root != null)
            {
                root.AppendChild(element);
            }
            else
            {
                var rootElement = doc.CreateElement("files");
                rootElement.AppendChild(element);
                doc.AppendChild(rootElement);
            }

            doc.Save(fileXml);
        }
    }
}