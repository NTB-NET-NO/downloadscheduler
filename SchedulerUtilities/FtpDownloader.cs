﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FtpDownloader.cs" company="Norsk Telegrambyrå AS">
//   Copyright (c) Norsk Telegrambyrå AS. All rights reserved.
// </copyright>
// <summary>
//   The ftp downloader.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace DownloadScheduler.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;

    using log4net;

    /// <summary>
    /// The ftp downloader.
    /// </summary>
    public class FtpDownloader
    {
        // TODO: We need some sort of logger to store which file and when we have downloaded it. If we see that the current file is newer, then we can download it again

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(FtpDownloader));

        /// <summary>
        /// Gets or sets the ftp username.
        /// </summary>
        public string FtpUsername { get; set; }

        /// <summary>
        /// Gets or sets the ftp password.
        /// </summary>
        public string FtpPassword { get; set; }

        /// <summary>
        /// Gets or sets the ftp server.
        /// </summary>
        public string FtpServer { get; set; }

        /// <summary>
        /// Gets or sets the ftp path.
        /// </summary>
        public string FtpPath { get; set; }

        /// <summary>
        /// Gets or sets the local paths.
        /// </summary>
        public List<string> LocalPaths { get; set; }

        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        public string Filter { get; set; }

        /// <summary>
        /// Gets or sets the filename.
        /// </summary>
        public string Filename { get; set; }

        /// <summary>
        /// Gets or sets the instance name.
        /// </summary>
        public string InstanceName { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unZip">
        /// The un Zip.
        /// </param>
        /// <param name="zipTarget">
        /// The zip Target.
        /// </param>
        public void Download(bool unZip, string zipTarget)
        {
            if (this.FtpUsername == string.Empty)
            {
                throw new Exception("Cannot connect to FTP server when we don't have an username");
            }

            if (this.FtpPassword == string.Empty)
            {
                throw new Exception("Cannot connect to FTP server when we don't have a password");
            }

            if (this.FtpServer == string.Empty)
            {
                throw new Exception("Cannot connect to FTP server when we don't know the name of the server");
            }

            if (this.FtpPath == string.Empty)
            {
                throw new Exception("Cannot connect to FTP server when we don't know the file path");
            }

            if (this.LocalPaths.Count == 0)
            {
                throw new Exception("Cannot connect to FTP server when we don't know where to store the file localy");
            }

            if (this.Filename == string.Empty)
            {
                throw new Exception("Cannot connect to FTP server when we don't know what to call the downloaded file");
            }

            // Get the object used to communicate with the server.

            // Connecting to the server
            FtpWebResponse response = null;
            StreamReader reader = null;

            List<string> fileList = new List<string>();

            try
            {
                FtpWebRequest request =
                    (FtpWebRequest)WebRequest.Create(@"ftp://" + this.FtpServer + @"/" + this.FtpPath);
                request.Method = WebRequestMethods.Ftp.ListDirectory;

                // request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(this.FtpUsername, this.FtpPassword);
                request.KeepAlive = false;
                request.UsePassive = false;
                request.UseBinary = false;
                request.Proxy = null;
                request.Timeout = 600000;

                response = (FtpWebResponse)request.GetResponse();

                // ReSharper disable AssignNullToNotNullAttribute
                reader = new StreamReader(response.GetResponseStream(), Encoding.Default);

                // ReSharper restore AssignNullToNotNullAttribute
                var line = reader.ReadLine();
                while (line != null)
                {
                    if (Regex.IsMatch(line, this.Filter))
                    {
                        fileList.Add(line.Trim());
                    }

                    line = reader.ReadLine();
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }

                if (response != null)
                {
                    response.Close();
                }
            }

            if (fileList.Count == 0)
            {
                Logger.Info("No files found matching filter" + this.Filter);
                return;
            }

            try
            {
                foreach (string file in fileList)
                {
                    Logger.Debug("Checking if file " + file + " is downloaded already");
                    var downloaded = false;
                    var currentFile = file;
                    if (currentFile.Contains(@"/"))
                    {
                        // Splitt and get the last one
                        var parts = file.Split('/');

                        currentFile = parts[parts.Count() - 1];
                    }

                    var request = (FtpWebRequest) WebRequest.Create(@"ftp://" + this.FtpServer + @"/" + this.FtpPath + @"/" + currentFile);
                    request.Method = WebRequestMethods.Ftp.GetDateTimestamp;

                    request.Credentials = new NetworkCredential(this.FtpUsername, this.FtpPassword);
                    request.KeepAlive = false;
                    request.UsePassive = false;
                    request.UseBinary = false;
                    request.Proxy = null;
                    request.Timeout = 600000;
                    DateTime lastModified;
                    using (response = (FtpWebResponse)request.GetResponse())
                    {
                        lastModified = response.LastModified;
                    }

                    // Now we have file age and filename of file
                    // Now we need to check this data somewhere
                    var fileDate = DateTime.Today.Year + DateTime.Today.Month.ToString().PadLeft(2, '0')
                                      + DateTime.Today.Day.ToString().PadLeft(2, '0');
                    //var fileXml = ConfigurationManager.AppSettings["DownloadedFilePath"] + fileDate + "_"
                    //                 + this.InstanceName + ".xml";
                    var fileXml = ConfigurationManager.AppSettings["DownloadedFilePath"] + "/" + this.InstanceName + ".xml";
                    var doc = new XmlDocument();

                    var xmlFileExists = new FileInfo(fileXml);
                    if (xmlFileExists.Exists)
                    {
                        doc.Load(fileXml);

                        // singleNode = doc.SelectSingleNode("//file[.='" + currentFile + "']");

                        // todo: Just check for filename first. Then for timestamp. Then we can add vers
                        var singleNode =
                            doc.SelectSingleNode("//file[@timestamp='" + lastModified.ToString(CultureInfo.InvariantCulture) + "'][.='" + currentFile + "']");

                        // If we cannot find this node, it means that we have not downloaded it
                        if (singleNode != null)
                        {
                            downloaded = true;
                            Logger.Info("File " + currentFile + " is downloaded already.");
                        }
                    }

                    response.Close();

                    if (downloaded)
                    {
                        continue;
                    }

                    Logger.Info("Downloading file " + file);
                    request = (FtpWebRequest) WebRequest.Create(@"ftp://" + this.FtpServer + @"/" + this.FtpPath + @"/" + currentFile);
                    request.Method = WebRequestMethods.Ftp.DownloadFile;

                    request.Credentials = new NetworkCredential(this.FtpUsername, this.FtpPassword);
                    request.KeepAlive = false;
                    request.UsePassive = false;
                    request.UseBinary = false;
                    request.Proxy = null;
                    request.Timeout = 600000;

                    response = (FtpWebResponse)request.GetResponse();

                    var responseStream = response.GetResponseStream();

                    var downloadFile = currentFile + ".tmp";
                    foreach (var localPath in this.LocalPaths)
                    {
                        var writeStream = new FileStream(localPath + @"\" + downloadFile, FileMode.Create);
                        const int length = 2048;
                        byte[] buffer = new byte[length];
                        if (responseStream != null)
                        {
                            var bytesRead = responseStream.Read(buffer, 0, length);
                            while (bytesRead > 0)
                            {
                                writeStream.Write(buffer, 0, bytesRead);
                                bytesRead = responseStream.Read(buffer, 0, length);
                            }
                        }

                        writeStream.Close();

                        // Renaming file to the original name
                        var fi = new FileInfo(localPath + @"\" + downloadFile);
                        fi.MoveTo(localPath + @"\" + currentFile);

                        if (unZip)
                        {
                            var unzip = new Unzip(localPath + @"\" + currentFile, zipTarget);
                        }
                    }

                    response.Close();

                    // Now we shall add this file to the datafile
                    XmlNode root = doc.DocumentElement;

                    var element = doc.CreateElement("file");
                    element.SetAttribute("timestamp", lastModified.ToString(CultureInfo.InvariantCulture));
                    element.SetAttribute("filename", currentFile);
                    element.InnerText = currentFile;

                    Logger.Info("Stroring information about download in xml-file");
                    if (root != null)
                    {
                        root.AppendChild(element);
                    }
                    else
                    {
                        var rootElement = doc.CreateElement("files");
                        rootElement.AppendChild(element);
                        doc.AppendChild(rootElement);
                    }

                    doc.Save(fileXml);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }

                if (response != null)
                {
                    response.Close();
                }
            }
        }
    }
}