using System;
using System.IO;
using Ionic.Zip;
using log4net;

namespace DownloadScheduler.Utilities
{
    class Unzip
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(Unzip));

        // public string Filename { get; set; }

        public Unzip(string outputFile, string outputPath)
        {
            Logger.Info("Unzipping: " + outputFile);
            using (ZipFile myZipFile = ZipFile.Read(outputFile))
            {
                if (this.DownloadZipTarget == null)
                {
                    this.DownloadZipTarget = outputPath;
                }
                foreach (ZipEntry entry in myZipFile)
                {
                    try
                    {

                        entry.Extract(this.DownloadZipTarget, ExtractExistingFileAction.OverwriteSilently);
                        Logger.Debug("Unzipping: " + this.DownloadZipTarget + @"\" + entry.FileName);
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception.Message);
                        Logger.Error(exception.StackTrace);
                    }
                }


            }
            Logger.Debug("Done unzipping: " + outputFile);
            // removing the downloaded file after we've unzipped the content
            try
            {
                // We might have to wait until the file becomes ready, but we can check for that...
                Logger.Info("Removing download file");
                FileInfo fi = new FileInfo(outputFile);
                fi.Delete();
                Logger.Debug("Done Removing download file");
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                Logger.Error(ex.StackTrace);
            }
        }

        protected string DownloadZipTarget { get; set; }
        
    }
}
