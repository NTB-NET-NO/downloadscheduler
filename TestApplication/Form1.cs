﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestApplication
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public string SetBasicAuthHeader(String userName, String userPassword)
        {
            string authInfo = userName + ":" + userPassword;
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            return "Basic " + authInfo;
        }

        private void btnGet_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUrl.Text))
            {
                return;
            }

            var url = txtUrl.Text;

            var request = (HttpWebRequest)WebRequest.Create(url);
            if (!string.IsNullOrEmpty(txtUsername.Text) && !string.IsNullOrEmpty(txtPassword.Text))
            {
                var userName = txtUsername.Text.Trim();
                var password = txtPassword.Text.Trim();
                request.Headers["Authorization"] = SetBasicAuthHeader(userName, password);
            }

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                var stringBuilder = new StringBuilder();
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    stringBuilder.Append(reader.ReadToEnd());
                    
                }

                txtOutput.Text = stringBuilder.ToString();
            }
        }
    }
}
