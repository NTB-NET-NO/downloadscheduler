﻿using System;
using System.Xml;
using DownloadScheduler.Components.Enums;

namespace DownloadScheduler.Components.Interfaces
{
    public interface ISchedulerComponent
    {
        /// <summary>
        /// Configure the component instance with all necessary data
        /// </summary>
        /// <param name="configNode"></param>
        void Configure(XmlNode configNode);

        /// <summary>
        /// Starts the polling for this instance
        /// </summary>
        void Start();

        /// <summary>
        /// Stops the polling for this instance
        /// </summary>
        void Stop();

        /// <summary>
        /// Gets the Enabled status of the instance.
        /// </summary>
        /// <value>The Enabled status.</value>
        Boolean Enabled { get; }


        /// <summary>
        /// Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        String InstanceName { get; }

        /// <summary>
        /// Gets the operation mode.
        /// </summary>
        /// <value>The operation mode.</value>
        OperationMode OperationMode { get; }

        /// <summary>
        /// Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        PollStyle PollStyle { get; }

        /// <summary>
        /// Gets the ComponentState
        /// </summary>
        ComponentState ComponentState { get; }

    }
}