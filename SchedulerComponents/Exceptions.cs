﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DownloadScheduler.Components
{
    
    class SchedulerComponentException : Exception
    {
             /// <summary>
        /// Initializes a new instance of the <see cref="SchedulerComponentException"/> class.
        /// </summary>
        /// <remarks>Specifies an error message only.</remarks>
        /// <param name="message">The error message</param>
        public SchedulerComponentException (string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SportsDataException"/> class.
        /// </summary>
        /// <remarks>
        /// Allows for specification of both an error message and an inner exception
        /// </remarks>
        /// <param name="message">The error message.</param>
        /// <param name="exception">Inner exception to wrap.</param>
        public SchedulerComponentException(string message, Exception exception)
            : base(message, exception)
        { }
    
    }
}
