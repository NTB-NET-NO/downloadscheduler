﻿namespace DownloadScheduler.Components
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Threading;
    using System.Xml;

    using DownloadScheduler.Components.Enums;
    using DownloadScheduler.Components.Interfaces;

    using log4net;

    using Quartz;
    using Quartz.Impl;
    using Quartz.Impl.Matchers;

    public partial class SchedulerComponent : Component, ISchedulerComponent
    {
        public SchedulerComponent()
        {
            this.InitializeComponent();
        }

        public SchedulerComponent(IContainer container)
        {
            container.Add(this);

            this.InitializeComponent();
        }

        public void Configure(XmlNode configNode)
        {
            
            Logger.Debug("Node: " + configNode.Name);

            const bool Purge = false;

            var filter = "";
            var ftpusername = "";
            var ftppassword = "";
            var ftppath = "";
            var ftpserver = "";
            var httpserver = "";
            var ziptarget = "";
            var filename = "";
            var extention = "";
            var unzip = false;

            this.DownloadPaths = new List<string>();

            if (configNode.Attributes != null)
            {
                Logger.Debug("Name attribut: " + configNode.Attributes.GetNamedItem("Name"));

                // Some values that we need.

                // This boolean variable is used to tell us that the database shall be populated or not

                //Basic configuration sanity check
                if (configNode.Name != "SchedulerComponent" || configNode.Attributes.GetNamedItem("Name") == null)
                {
                    throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
                }

                if (Thread.CurrentThread.Name == null)
                {
                    if (configNode.Attributes != null)
                    {
                        Thread.CurrentThread.Name = configNode.Attributes.GetNamedItem("Name").ToString();
                    }
                }

                #region Basic config

                // Getting the name of this Component instance
                try
                {
                    if (configNode.Attributes != null)
                    {
                        this.Name = configNode.Attributes["Name"].Value;
                        this.enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
                    }
                    ThreadContext.Stacks["NDC"].Push(this.InstanceName);
                }
                catch (Exception ex)
                {
                    Logger.Fatal("Not possible to configure this job instance!", ex);
                }

                //Basic operation
                try
                {
                    if (configNode.Attributes != null)
                    {
                        this.operationMode =
                            (OperationMode)
                            Enum.Parse(typeof(OperationMode), configNode.Attributes["OperationMode"].Value, true);
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
                }

                try
                {
                    if (configNode.Attributes != null)
                    {
                        this.pollStyle =
                            (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing PollStyle values in XML configuration node", ex);
                }

                try
                {
                    if (configNode.Attributes != null)
                    {
                        this.ScheduleType =
                            (ScheduleType)
                            Enum.Parse(typeof(ScheduleType), configNode.Attributes["ScheduleType"].Value, true);
                    }
                    // ScheduleType = (ScheduleT
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing ScheduleType values in XML configuration node", ex);
                }

                try
                {
                    if (configNode.Attributes != null && configNode.Attributes["Unzip"] != null)
                    {
                        unzip = Convert.ToBoolean(configNode.Attributes["Unzip"].Value);
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing ScheduleType values in XML configuration node", ex);
                }

                #endregion
            }

            if (this.enabled)
            {
                #region Set up Download

                var xmlNode =
                    configNode.SelectSingleNode("../SchedulerComponent[@Name='" + this.InstanceName + "']/Download");

                if (xmlNode != null)
                {
                    if (xmlNode.Attributes != null)
                    {
                        this.DownloadType =
                            (DownloadType)Enum.Parse(typeof(DownloadType), xmlNode.Attributes["type"].Value, true);

                        if (this.DownloadType == DownloadType.Http)
                        {
                            if (xmlNode.Attributes["path"] != null)
                            {
                                httpserver = xmlNode.Attributes["path"].Value;
                            }

                            if (xmlNode.Attributes["username"] != null)
                            {
                                ftpusername = xmlNode.Attributes["username"].Value;
                            }

                            if (xmlNode.Attributes["password"] != null)
                            {
                                ftppassword = xmlNode.Attributes["password"].Value;
                            }
                        }
                        if (this.DownloadType == DownloadType.Ftp)
                        {
                            if (xmlNode.Attributes["filter"] != null)
                            {
                                filter = xmlNode.Attributes["filter"].Value;
                            }

                            if (xmlNode.Attributes["username"] != null)
                            {
                                ftpusername = xmlNode.Attributes["username"].Value;
                            }

                            if (xmlNode.Attributes["password"] != null)
                            {
                                ftppassword = xmlNode.Attributes["password"].Value;
                            }

                            if (xmlNode.Attributes["path"] != null)
                            {
                                ftpserver = xmlNode.Attributes["path"].Value;
                            }

                            if (xmlNode.Attributes["filepath"] != null)
                            {
                                ftppath = xmlNode.Attributes["filepath"].Value;
                            }
                        }
                    }
                }

                #endregion

                #region FileDownload path

                XmlNodeList xmlFolderNodes = configNode.SelectNodes("Folders/Folder[@type='OutputFolder']");

                if (xmlFolderNodes != null)
                {
                    foreach (XmlNode xmlFolderNode in xmlFolderNodes)
                    {
                        this.DownloadPaths.Add(xmlFolderNode.InnerText);
                    }
                }

                xmlNode = configNode.SelectSingleNode("Folders");

                if (xmlNode != null)
                {
                    if (xmlNode.Attributes != null)
                    {
                        if (xmlNode.Attributes["filename"] != null)
                        {
                            filename = xmlNode.Attributes["filename"].Value;
                        }

                        if (xmlNode.Attributes["extention"] != null)
                        {
                            extention = xmlNode.Attributes["extention"].Value;
                        }
                    }
                }

                // Let's also get the Ziptarget element
                xmlNode = configNode.SelectSingleNode("Ziptarget");

                if (unzip)
                {
                    if (xmlNode != null)
                    {
                        if (xmlNode.Attributes != null)
                        {
                            ziptarget = xmlNode.Attributes["path"].Value;
                        }
                    }
                }

                #endregion

                // Creating Instance Name

                #region Set up polling

                try
                {
                    //Switch on pollstyle
                    switch (this.pollStyle)
                    {
                            #region pollStyle.CalendarPoll

                        case PollStyle.CalendarPoll:
                            // Getting the times for this job

                            try
                            {
                                IJobDetail weeklyJobDetail = null;
                                // = JobBuilder.Create<TournamentMatches>().WithIdentity("Job1", "Group1").Build();

                                ITrigger weeklyTrigger = null;

                                Logger.Info("Setting up schedulers");
                                Logger.Info("--------------------------------------------------------------");

                                // Getting the number of jobs to create
                                var nodeList = configNode.SelectNodes("Schedules/Schedule");

                                var jobs = 0;
                                if (nodeList != null)
                                {
                                    jobs = nodeList.Count;
                                }
                                if (nodeList != null)
                                {
                                    Logger.Debug("Number of jobs: " + jobs);
                                }

                                // Creating a string to store the ScheduleID
                                string scheduleId = null;

                                // Creating a boolean variable to store if this is a schedule-message og not
                                const bool ScheduleMessage = false;

                                Logger.Debug("Schedule of type: " + this.ScheduleType);

                                // Getting the scheduler
                                var scheduler = this.SchedulerFactory.GetScheduler();

                                switch (this.ScheduleType)
                                {
                                    case ScheduleType.Hourly:
                                        // We are then only to get the minue from the config-file
                                        if (nodeList != null)
                                        {
                                            foreach (
                                                var node in
                                                    nodeList.Cast<XmlNode>().Where(node => node.Attributes != null))
                                            {
                                                if (node.Attributes["id"] != null)
                                                {
                                                    scheduleId = node.Attributes["id"].Value;
                                                }
                                                var scheduleMinute = Convert.ToInt32(node.Attributes["minute"].Value);

                                                // Creating the daily cronexpression
                                                var stringCronExpression = "0 " + scheduleMinute + " " + "* " + "? "
                                                                              + "* " + "*";

                                                //"0 " +
                                                //                              scheduleTimeArray[1] + " " +
                                                //                              scheduleTimeArray[0] + " " +
                                                //                              "? " +
                                                //                              "* " +
                                                //                              "* ";

                                                Logger.Debug("CronExpression: " + stringCronExpression);

                                                // dailyCronTrigger = new CronTrigger(InstanceName + ScheduleID, InstanceName);
                                                // dailyCronTrigger.CronExpression = cronExpression;

                                                var hourlyCronTrigger =
                                                    TriggerBuilder.Create()
                                                        .WithIdentity(this.InstanceName + "_" + scheduleId, "Group1")
                                                        .WithDescription(this.InstanceName)
                                                        .WithCronSchedule(stringCronExpression)
                                                        .Build();

                                                Logger.Debug("dailyCronTrigger: " + hourlyCronTrigger.Description);

                                                //// Creating the jobDetail 
                                                //IJobDetail hourlyJobDetail =
                                                //    JobBuilder.Create<DownloadSchedulerJob>()
                                                //              .WithIdentity(
                                                //                  "job_" + InstanceName + "_" + scheduleId, "group1")
                                                //              .WithDescription(InstanceName)
                                                //              .Build();

                                                //switch (DownloadType)
                                                //{
                                                //    case DownloadType.Ftp:
                                                //        hourlyJobDetail.JobDataMap["FtpUsername"] = ftpusername;
                                                //        hourlyJobDetail.JobDataMap["FtpPassword"] = ftppassword;
                                                //        hourlyJobDetail.JobDataMap["FtpServer"] = ftpserver;
                                                //        hourlyJobDetail.JobDataMap["FtpPath"] = ftppath;
                                                //        hourlyJobDetail.JobDataMap["LocalPath"] = downloadPath;
                                                //        hourlyJobDetail.JobDataMap["Filename"] = filter;
                                                //        hourlyJobDetail.JobDataMap["DownloadType"] = DownloadType.Ftp.ToString();
                                                //        break;

                                                //    case DownloadType.Http:
                                                //        hourlyJobDetail.JobDataMap["HttpServer"] = httpserver;
                                                //        hourlyJobDetail.JobDataMap["LocalPath"] = downloadPath;
                                                //        hourlyJobDetail.JobDataMap["DownloadType"] = DownloadType.Http.ToString();
                                                //        break;
                                                //}

                                                var hourlyJobDetail = this.SetJobDetails(
                                                    scheduleId,
                                                    ftpusername,
                                                    ftppassword,
                                                    ftpserver,
                                                    ftppath,
                                                    this.DownloadPaths,
                                                    filter,
                                                    httpserver,
                                                    unzip,
                                                    ziptarget,
                                                    filename + @"." + extention,
                                                    this.InstanceName);

                                                if (hourlyJobDetail != null)
                                                {
                                                    // dailyJobDetail.AddJobListener(JobListenerName);

                                                    Logger.Debug(
                                                        "Setting up and starting dailyJobDetail job "
                                                        + hourlyJobDetail.Description + " using trigger : "
                                                        + hourlyCronTrigger.Description);
                                                    // scheduler.ScheduleJob(dailyJobDetail, dailyCronTrigger[a]);
                                                    scheduler.ScheduleJob(hourlyJobDetail, hourlyCronTrigger);
                                                }
                                            }
                                        }

                                        break;
                                    case ScheduleType.Daily:

                                        string hour = "";
                                        string minute = "";

                                        if (nodeList != null)
                                        {
                                            foreach (XmlNode node in nodeList)
                                            {
                                                if (node.Attributes != null)
                                                {
                                                    if (node.Attributes["id"] != null)
                                                    {
                                                        scheduleId = node.Attributes["id"].Value;
                                                    }

                                                    // We have a new structure for schedule

                                                    if (node.Attributes["hour"] != null)
                                                    {
                                                        hour = node.Attributes["hour"].Value;
                                                    }

                                                    if (node.Attributes["minute"] != null)
                                                    {
                                                        minute = node.Attributes["minute"].Value;
                                                    }
                                                }

                                                // If minutes contains two zeros (00), we change it to one zero (0) 
                                                // If not, scheduler won't understand
                                                if (minute == "00")
                                                {
                                                    minute = "0";
                                                }

                                                // Doing the same thing for hours
                                                if (hour == "00")
                                                {
                                                    hour = "0";
                                                }

                                                /*
                                             * Creating cron expressions and more. 
                                             * This is the cron syntax:
                                             *  Seconds
                                             *  Minutes
                                             *  Hours
                                             *  Day-of-Month
                                             *  Month
                                             *  Day-of-Week
                                             *  Year (optional field)
                                             */

                                                // Creating the daily cronexpression
                                                string stringCronExpression = "0 " + minute + " " + hour + " " + "? "
                                                                              + "* " + "* ";

                                                // Setting up the CronTrigger
                                                Logger.Debug(
                                                    "Setting up the CronTrigger with following pattern: "
                                                    + stringCronExpression);

                                                // dailyCronTrigger = new CronTrigger(InstanceName + ScheduleID, InstanceName);
                                                // dailyCronTrigger.CronExpression = cronExpression;
                                                ITrigger dailyCronTrigger =
                                                    TriggerBuilder.Create()
                                                        .WithIdentity(this.InstanceName + "_" + scheduleId, "Group1")
                                                        .WithDescription(this.InstanceName)
                                                        .WithCronSchedule(stringCronExpression)
                                                        .Build();

                                                Logger.Debug("dailyCronTrigger: " + dailyCronTrigger.Description);

                                                var dailyJobDetail = this.SetJobDetails(
                                                    scheduleId,
                                                    ftpusername,
                                                    ftppassword,
                                                    ftpserver,
                                                    ftppath,
                                                    this.DownloadPaths,
                                                    filter,
                                                    httpserver,
                                                    unzip,
                                                    ziptarget,
                                                    filename + @"." + extention,
                                                    this.InstanceName);

                                                if (dailyJobDetail != null)
                                                {
                                                    // dailyJobDetail.AddJobListener(JobListenerName);

                                                    Logger.Debug(
                                                        "Setting up and starting dailyJobDetail job "
                                                        + dailyJobDetail.Description + " using trigger : "
                                                        + dailyCronTrigger.Description);
                                                    // scheduler.ScheduleJob(dailyJobDetail, dailyCronTrigger[a]);
                                                    scheduler.ScheduleJob(dailyJobDetail, dailyCronTrigger);
                                                }
                                            }
                                        }
                                        break;

                                    case ScheduleType.Weekly:
                                        if (nodeList != null)
                                        {
                                            foreach (XmlNode node in nodeList)
                                            {
                                                if (node.Attributes["id"] != null)
                                                {
                                                    scheduleId = node.Attributes["id"].Value;
                                                }

                                                // Splitting the time into two values so that we can create a cron job
                                                var scheduleTimeArray =
                                                    node.Attributes["Time"].Value.Split(new[] { ':' });

                                                // If minutes contains two zeros (00), we change it to one zero (0) 
                                                // If not, scheduler won't understand
                                                if (scheduleTimeArray[1] == "00")
                                                {
                                                    scheduleTimeArray[1] = "0";
                                                }
                                                int minutes = Convert.ToInt32(scheduleTimeArray[1]);

                                                // Doing the same thing for hours
                                                if (scheduleTimeArray[0] == "00")
                                                {
                                                    scheduleTimeArray[0] = "0";
                                                }
                                                int hours = Convert.ToInt32(scheduleTimeArray[0]);

                                                string purgeWeeks = "";
                                                if (node.Attributes["Week"] != null)
                                                {
                                                    purgeWeeks = node.Attributes["Week"].Value;
                                                }

                                                string scheduleDay = "";
                                                if (node.Attributes["DayOfWeek"] != null)
                                                {
                                                    scheduleDay = node.Attributes["DayOfWeek"].Value;
                                                }

                                                // If we use the DateTimeOffset it means that we have to find out if the day has passed or not.
                                                // So we have to find out which day it is today
                                                DayOfWeek today = DateTime.Today.DayOfWeek;
                                                int numDayOfWeek = 0;
                                                switch (today.ToString())
                                                {
                                                    case "Monday":
                                                        numDayOfWeek = 1;
                                                        break;
                                                    case "Tuesday":
                                                        numDayOfWeek = 2;
                                                        break;

                                                    case "Wednesday":
                                                        numDayOfWeek = 3;
                                                        break;

                                                    case "Thursday":
                                                        numDayOfWeek = 4;
                                                        break;

                                                    case "Friday":
                                                        numDayOfWeek = 5;
                                                        break;

                                                    case "Saturday":
                                                        numDayOfWeek = 6;
                                                        break;

                                                    case "Sunday":
                                                        numDayOfWeek = 0;
                                                        break;
                                                }

                                                // Checking if numDayOfWeek is smaller than ScheduleDay
                                                DateTimeOffset dto = DateTime.Today;

                                                // Get the scheduled day
                                                int scheduledDay = Convert.ToInt32(scheduleDay);

                                                int days;
                                                if (numDayOfWeek <= scheduledDay)
                                                {
                                                    // It is, so we need to find out when the next scheduling should happen
                                                    days = scheduledDay - numDayOfWeek; // this can be zero

                                                    dto =
                                                        DateTime.Today.AddDays(days).AddHours(hours).AddMinutes(minutes);

                                                    // <Schedule Id="1" Time="15:12" DayOfWeek="1" Week="2"/>
                                                }
                                                else if (numDayOfWeek > scheduledDay)
                                                {
                                                    const int Weekdays = 7;
                                                    int daysLeft = Weekdays - numDayOfWeek;
                                                    days = daysLeft + scheduledDay;

                                                    dto =
                                                        DateTime.Today.AddDays(days).AddHours(hours).AddMinutes(minutes);
                                                }
                                                // Creating a simple Scheduler
                                                CalendarIntervalScheduleBuilder calendarIntervalSchedule =
                                                    CalendarIntervalScheduleBuilder.Create();

                                                // Creating the weekly Trigger using the stuff that we've calculated
                                                weeklyTrigger =
                                                    TriggerBuilder.Create()
                                                        .WithDescription(this.InstanceName)
                                                        .WithIdentity(this.InstanceName + "_" + scheduleId, "Group1")
                                                        .StartAt(dto)
                                                        .WithSchedule(
                                                            calendarIntervalSchedule.WithIntervalInWeeks(
                                                                Convert.ToInt32(purgeWeeks)))
                                                        .Build();

                                                // This part might be moved
                                                if (this.OperationMode == OperationMode.Purge)
                                                {
                                                    weeklyJobDetail =
                                                        JobBuilder.Create<DownloadSchedulerJob>()
                                                            .WithIdentity(
                                                                "job_" + this.InstanceName + "_" + scheduleId,
                                                                "group1")
                                                            .WithDescription(this.InstanceName)
                                                            .Build();
                                                }

                                                weeklyJobDetail.JobDataMap["ScheduleType"] =
                                                    this.ScheduleType.ToString();
                                                weeklyJobDetail.JobDataMap["ScheduleMessage"] = ScheduleMessage;
                                                weeklyJobDetail.JobDataMap["Weeks"] = purgeWeeks;
                                                weeklyJobDetail.JobDataMap["Purge"] = Purge;
                                            }
                                        }

                                        if (weeklyJobDetail != null)
                                        {
                                            Logger.Debug(
                                                "Setting up and starting weeklyJobDetail job "
                                                + weeklyJobDetail.Description + " using trigger : "
                                                + weeklyTrigger.Description);
                                            scheduler.ScheduleJob(weeklyJobDetail, weeklyTrigger);
                                        }

                                        break;

                                    case ScheduleType.Monthly:
                                        // doing Monthly stuff here
                                        break;

                                    case ScheduleType.Yearly:
                                        // doing yearly Stuff here
                                        break;
                                }
                            }
                            catch (SchedulerConfigException schedulerConfigException)
                            {
                                Logger.Debug("A SchedulerConfigException has occured: ", schedulerConfigException);
                            }
                            catch (SchedulerException schedulerException)
                            {
                                Logger.Debug("A schedulerException has occured: ", schedulerException);
                            }
                            catch (Exception exception)
                            {
                                Logger.Debug("An exception has occured", exception);
                            }

                            break;

                            #endregion

                            #region PollStyle.Scheduled

                        case PollStyle.Scheduled:
                            /* 
                            schedule = configNode.Attributes["Schedule"].Value;
                            TimeSpan s = Utilities.GetScheduleInterval(schedule);
                            logger.DebugFormat("Schedule: {0} Calculated time to next run: {1}", schedule, s);
                            pollTimer.Interval = s.TotalMilliseconds;
                            */
                            throw new NotSupportedException("Invalid polling style for this job type");

                            #endregion

                            #region PollStyle.FileSystemWatch

                        case PollStyle.FileSystemWatch:
                            throw new NotSupportedException("Invalid polling style for this job type");

                            #endregion

                        default:
                            //Unsupported pollstyle for this object
                            throw new NotSupportedException(
                                "Invalid polling style for this job type (" + this.PollStyle + ")");
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException(
                        "Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message,
                        ex);
                }

                #endregion
            }
            //Finish configuration
            ThreadContext.Stacks["NDC"].Pop();
            this.Configured = true;
        }

        public void Start()
        {
            if (!this.Configured)
            {
                throw new SchedulerComponentException(
                    "SchedulerComponent is not properly Configured. SchedulerComponent::Start() Aborted!");
            }

            if (!this.enabled)
            {
                throw new SchedulerComponentException(
                    "SchedulerComponent is not Enabled. SchedulerComponent::Start() Aborted!");
            }

            // Whatever we do, we must check two database-tables and see if they are populated or not
            // These are:
            // Matches

            // Starting up the different pollstyles
            try
            {
                // Continous mode means that we are checking on an interval
                if (this.pollStyle == PollStyle.Continous)
                {
                    // Not sure if we have to do anything
                    Logger.Info("Starting Continous PollStyle component");
                    this.pollTimer.Enabled = true;
                }
                if (this.pollStyle == PollStyle.CalendarPoll)
                {
                    Logger.Info("Setting up jobListener and scheduler");

                    // starting the scheduler
                    var scheduler = this.SchedulerFactory.GetScheduler();

                    IList<string> jobGroupNames = scheduler.GetJobGroupNames();

                    foreach (string jobGroupName in jobGroupNames)
                    {
                        if (jobGroupName.ToLower() == "groupscheduler")
                        {
                            Logger.Debug("JobGroupName: " + jobGroupName);

                            Logger.Debug(
                                "Key: " + scheduler.GetJobKeys(GroupMatcher<JobKey>.GroupContains(jobGroupName)));

                            var groupMatcher = GroupMatcher<JobKey>.GroupContains(jobGroupName);
                            var jobKeys = scheduler.GetJobKeys(groupMatcher);

                            foreach (JobKey jobKey in jobKeys)
                            {
                                Logger.Debug("Name: " + jobKey.Name);
                                Logger.Debug("Group: " + jobKey.Group);
                            }
                        }
                    }

                    IList<string> triggerGroupNames = scheduler.GetTriggerGroupNames();

                    foreach (string triggerGroupName in triggerGroupNames)
                    {
                        Logger.Debug("TriggerGroupName: " + triggerGroupName);
                    }

                    IList<string> jobs = scheduler.GetJobGroupNames();
                    foreach (string job in jobs)
                    {
                        Logger.Debug("Job: " + job);
                    }

                    Logger.Debug("This Component name: " + this.Name);

                    // Starting scheduler
                    scheduler.Start();
                }
            }
            catch (SchedulerException se)
            {
                Logger.Debug(se);
            }

            this.stopEvent.Reset();
            this.busyEvent.Set();

            // Starting poll-timer
            // pollTimer.Interval = 2000;

            // Setting ComponentState to running
            this.ComponentState = ComponentState.Running;

            this.pollTimer.Start();
        }

        public void Stop()
        {
            if (!this.Configured)
            {
                throw new SchedulerComponentException(
                    "SchedulerComponent is not properly Configured. SchedulerComponent::Stop() Aborted");
            }

            if (!this.enabled)
            {
                throw new SchedulerComponentException(
                    "SchedulerComponent is not properly Configured. SchedulerComponent::Stop() Aborted");
            }

            #region DEBUG

            Logger.Debug(this.pollTimer.Enabled == false ? "pollTimer is disabled" : "pollTimer is Enabled");

            const double Epsilon = 0;
            Logger.Debug(
                Math.Abs(this.pollTimer.Interval - 5000) < Epsilon
                    ? "pollTimer has an interval of 5000 milliseconds"
                    : "pollTimer DOES NOT have an interval of 5000 milliseconds");
            Logger.Debug(this.ErrorRetry ? "We are in ErrorRetry mode!" : "We are NOT in ErrorRetry mode!");

            Logger.Debug(
                this.pollStyle == PollStyle.FileSystemWatch
                    ? "pollStyle is of style FileSystemWatch"
                    : "pollStyle is NOT of style FileSystemWatch");

            #endregion

            #region Stopping Scheduler

            // Check status - Handle busy polltimer loops
            if (this.pollStyle == PollStyle.CalendarPoll)
            {
                try
                {
                    Logger.Debug("Deleting / stopping jobs!");
                    ISchedulerFactory sf = new StdSchedulerFactory();
                    
                    var sched = sf.GetScheduler();

                    // Shutting down scheduler
                    sched.GetJobGroupNames();

                    var keys = new List<JobKey>(sched.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(this.InstanceName)));
                    // Looping through each key as we'd like to know when things go right and not!
                    keys.ForEach(
                        key =>
                            {
                                var detail = sched.GetJobDetail(key);
                                sched.DeleteJob(key);

                                Logger.Debug(
                                    "Shutting down: " + detail.Description + "(" + key.Name + ")" + " Group: "
                                    + key.Group);
                            });

                    sched.Shutdown();
                }
                catch (SchedulerException sex)
                {
                    Logger.Fatal(sex);
                }

                catch (Exception ex)
                {
                    Logger.Error("Problems closing NFF WebClients!", ex);
                }
            }

            #endregion

            if (!this.pollTimer.Enabled
                && (this.ErrorRetry || this.pollStyle != PollStyle.FileSystemWatch
                    || Math.Abs(this.pollTimer.Interval - 5000) < Epsilon))
            {
                Logger.InfoFormat("Waiting for instance to complete work. Job {0}", this.InstanceName);
            }

            // Signal Stop
            this.stopEvent.Set();

            // Stop events
            this.filesWatcher.EnableRaisingEvents = false;

            if (!this.busyEvent.WaitOne(30000))
            {
                Logger.InfoFormat(
                    "Instance did not complete properly. Data may be inconsistent. Job: {0}",
                    this.InstanceName);
            }

            this.ComponentState = ComponentState.Halted;
            // Kill polling
            this.pollTimer.Stop();
        }

        /// <summary>
        /// Gets the Enabled status of the instance.
        /// </summary>
        /// <value>The Enabled status.</value>
        /// <remarks><c>True</c> if the job instance is Enabled.</remarks>
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
        }

        /// <summary>
        /// Gets the name of the instance.
        /// </summary>
        /// <value>The name of the instance.</value>
        /// <remarks>The Configured instance job name.</remarks>
        public string InstanceName
        {
            get
            {
                return this.Name;
            }
        }

        /// <summary>
        /// Gets the operation mode. 
        /// </summary>
        /// <value>The operation mode.</value>
        /// <remarks><c>Gatherer</c> is the only valid mode, its hard coded for this component.</remarks>
        public OperationMode OperationMode
        {
            get
            {
                return OperationMode.Gatherer;
            }
        }

        /// <summary>
        /// Gets the poll style.
        /// </summary>
        /// <value>The poll style.</value>
        /// <remarks>Contionous, Scheduled and FileSystemWatch are valid for <c>Gatherer</c> objects.</remarks>
        public PollStyle PollStyle
        {
            get
            {
                return this.pollStyle;
            }
        }

        public ComponentState ComponentState { get; set; }

        private IJobDetail SetJobDetails(
            string scheduleId,
            string ftpusername,
            string ftppassword,
            string ftpserver,
            string ftppath,
            List<string> downloadPaths,
            string filter,
            string httpserver,
            bool unzip,
            string ziptarget,
            string filename,
            string instanceName)
        {
            // Creating the jobDetail 
            IJobDetail jobDetail =
                JobBuilder.Create<DownloadSchedulerJob>()
                    .WithIdentity("job_" + this.InstanceName + "_" + scheduleId, "group1")
                    .WithDescription(this.InstanceName)
                    .Build();

            switch (this.DownloadType)
            {
                case DownloadType.Ftp:
                    jobDetail.JobDataMap["FtpUsername"] = ftpusername;
                    jobDetail.JobDataMap["FtpPassword"] = ftppassword;
                    jobDetail.JobDataMap["FtpServer"] = ftpserver;
                    jobDetail.JobDataMap["FtpPath"] = ftppath;
                    jobDetail.JobDataMap["LocalPaths"] = downloadPaths;
                    jobDetail.JobDataMap["Filter"] = filter;
                    jobDetail.JobDataMap["Filename"] = filename;
                    jobDetail.JobDataMap["DownloadType"] = DownloadType.Ftp.ToString();
                    jobDetail.JobDataMap["UnZip"] = unzip;
                    jobDetail.JobDataMap["ZipTarget"] = ziptarget;
                    jobDetail.JobDataMap["InstanceName"] = instanceName;
                    break;

                case DownloadType.Http:
                    jobDetail.JobDataMap["HttpServer"] = httpserver;
                    jobDetail.JobDataMap["LocalPaths"] = downloadPaths;
                    jobDetail.JobDataMap["DownloadType"] = DownloadType.Http.ToString();
                    jobDetail.JobDataMap["UnZip"] = unzip;
                    jobDetail.JobDataMap["ZipTarget"] = ziptarget;
                    jobDetail.JobDataMap["Filename"] = filename;
                    jobDetail.JobDataMap["InstanceName"] = instanceName;
                    jobDetail.JobDataMap["HttpUsername"] = ftpusername;
                    jobDetail.JobDataMap["HttpPassword"] = ftppassword;
                    break;
            }
            return jobDetail;
        }

        #region Logging

        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SchedulerComponent));

        /// <summary>
        /// Initializes the <see>
        ///                     <cref>GathererComponent</cref>
        ///                 </see>
        ///     class.
        /// </summary>
        static SchedulerComponent()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        #endregion

        #region Common class variables

        protected ScheduleType ScheduleType;

        protected DownloadType DownloadType;

        private readonly AutoResetEvent busyEvent = new AutoResetEvent(true);

        /// <summary>
        /// Exit control event
        /// </summary>
        private readonly AutoResetEvent stopEvent = new AutoResetEvent(false);

        /// <summary>
        /// The Configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName"/></remarks>
        protected String Name;

        /// <summary>
        /// Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly Configured
        /// </summary>
        /// <remarks>Holds the Configured status of the instance. <c>True</c> means successfully Configured</remarks>
        protected Boolean Configured;

        /// <summary>
        /// Error-Retry flag
        /// </summary>
        /// <remarks>The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later retry.</remarks>
        protected Boolean ErrorRetry = false;

        /// <summary>
        /// Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        protected String EmailNotification;

        /// <summary>
        /// Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        protected String EmailSubject;

        /// <summary>
        /// Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        protected String EmailBody;

        protected List<string> DownloadPaths;

        #endregion

        #region Polling settings

        /// <summary>
        /// schedulerFactory to be used to create scheduled tasks
        /// </summary>
        protected StdSchedulerFactory SchedulerFactory = new StdSchedulerFactory();

        protected IScheduler Scheduler;

        /// <summary>
        /// The Configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///   <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///   <para>Default value: <c>60</c></para>
        /// </remarks>
        protected int Interval = 60;

        #endregion
    }
}