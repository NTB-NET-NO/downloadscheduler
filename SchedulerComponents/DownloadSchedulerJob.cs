﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DownloadSchedulerJob.cs" company="Norsk Telegrambyrå">
//   Copyright NTB AS
// </copyright>
// <summary>
//   The download scheduler job.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DownloadScheduler.Components
{
    using System;
    using System.Collections.Generic;

    using DownloadScheduler.Utilities;

    using log4net;

    using Quartz;

    /// <summary>
    /// The download scheduler job.
    /// </summary>
    internal class DownloadSchedulerJob : IJob
    {
        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(DownloadSchedulerJob));

        /// <summary>
        /// Initializes static members of the <see cref="DownloadSchedulerJob"/> class.
        /// </summary>
        static DownloadSchedulerJob()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }
        }

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public void Execute(IJobExecutionContext context)
        {
            Logger.Info("Executing JobExecutionContext" + context.JobDetail.Description);
            var downloadType = string.Empty;
            var unZip = false;
            var zipTarget = string.Empty;
            var instanceName = string.Empty;

            // Mapping information sent from Component
            var dataMap = context.JobDetail.JobDataMap;

            try
            {
                downloadType = dataMap.GetString("DownloadType");
                unZip = dataMap.GetBoolean("UnZip");
                zipTarget = dataMap.GetString("ZipTarget");
                instanceName = dataMap.GetString("InstanceName");
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);
            }

            var filter = string.Empty;
            // Populate variables to be used to 
            try
            {
                // Cleaning up the code a bit. having all variable implementations here
                List<string> localPaths;
                switch (downloadType.ToLower())
                {
                    case "ftp":

                        var ftpUsername = dataMap.GetString("FtpUsername");
                        var ftpPassword = dataMap.GetString("FtpPassword");
                        var ftpServer = dataMap.GetString("FtpServer");
                        var ftpPath = dataMap.GetString("FtpPath");
                        localPaths = (List<string>)dataMap["LocalPaths"];
                        filter = dataMap.GetString("Filter");
                        
                        var year = DateTime.Today.Year.ToString().PadLeft(4, '0');
                        var month = DateTime.Today.Month.ToString().PadLeft(2, '0');
                        var day = DateTime.Today.Day.ToString().PadLeft(2, '0');

                        filter = filter.Replace("YEAR", year).Replace("MONTH", month).Replace("DAY", day);

                        var ftpDownloader = new FtpDownloader
                                                          {
                                                              FtpPassword = ftpPassword, 
                                                              FtpUsername = ftpUsername, 
                                                              FtpServer = ftpServer, 
                                                              FtpPath = ftpPath, 
                                                              Filter = filter, 
                                                              LocalPaths = localPaths, 
                                                              InstanceName = instanceName
                                                          };

                        // Do the actual download
                        ftpDownloader.Download(unZip, zipTarget);

                        break;

                    case "http":
                        var httpServer = dataMap.GetString("HttpServer");
                        localPaths = (List<string>)dataMap["LocalPaths"];
                        var filename = dataMap.GetString("Filename");
                        var username = dataMap.GetString("HttpUsername");
                        var password = dataMap.GetString("HttpPassword");
                        filter = dataMap.GetString("Filter");
                        var httpDownloader = new HttpDownloader
                                                            {
                                                                Server = httpServer, 
                                                                LocalPaths = localPaths, 
                                                                Filename = filename, 
                                                                Username = username, 
                                                                Password = password,
                                                                InstanceName = instanceName,
                                                                Filter = filter
                                                            };

                        httpDownloader.Download();
                        break;
                }
            }
            catch (Exception exception)
            {
                Logger.Debug(exception.Message);
                Logger.Debug(exception.StackTrace);
            }
        }
    }
}