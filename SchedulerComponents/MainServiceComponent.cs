﻿namespace DownloadScheduler.Components
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.ServiceModel;
    using System.Threading;
    using System.Xml;

    using DownloadScheduler.Components.Interfaces;
    using DownloadScheduler.Utilities;

    using log4net;

    public partial class MainServiceComponent : Component
    {
        private static readonly ILog Logger;

        static MainServiceComponent()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }

            Logger = LogManager.GetLogger(typeof(MainServiceComponent));

            if (Thread.CurrentThread.Name == null)
            {
                Thread.CurrentThread.Name = "MainServiceComponent";
            }
        }

        /// <summary>
        /// Initializes a new instance of the class
        /// </summary>
        public MainServiceComponent()
        {
            this.InitializeComponent();
        }

        public MainServiceComponent(IContainer container)
        {
            container.Add(this);

            this.InitializeComponent();
        }

        private void ConfigFileWatcherChanged(object sender, FileSystemEventArgs e)
        {
            ThreadContext.Properties["JOBNAME"] = "MainServiceWorker";

            // Outer-try-final to prevent dupe events
            try
            {
                // Stopping file watcher
                this.configFileWatcher.EnableRaisingEvents = false;
                Logger.Info("MainServiceComponent::configFileWatcher_Changed() hit. Will reconfigure.");

                ThreadContext.Stacks["NDC"].Push("RECONFIGURE");
                // Pause everything
                this.Pause();

                // Give it a little break
                Thread.Sleep(5000);

                // Reconfigure
                ConfigurationManager.RefreshSection("applicationSettings");
                ConfigurationManager.RefreshSection("appSection");
                this.Configure();

                this.Start();
            }
            catch (Exception ex)
            {
                Logger.Fatal("NTBSportsData reconfiguration failed - TERMINATING!", ex);
                Mail.SendException(ex);
                throw;
            }

            finally
            {
                ThreadContext.Stacks["NDC"].Pop();
                this.configFileWatcher.EnableRaisingEvents = this.WatchConfigSet;
            }
        }

        #region Instance variables and control data

        /// <summary>
        /// Name of the file that contains the job configuration set
        /// </summary>
        /// <remarks>The name of the job config XML file is stored here.</remarks>
        protected String ConfigSet = String.Empty;

        /// <summary>
        /// Enable watching the config set for changes and reconfigure at runtime
        /// </summary>
        /// <remarks>config setting that defines if we are wathcing the config set for changes/auto reconfigure or not.</remarks>
        protected Boolean WatchConfigSet;

        /// <summary>
        /// List of currently Configured jobs
        /// </summary>
        /// <remarks>Internal Dictionary to keep track of running worker jobs.</remarks>
        protected Dictionary<String, ISchedulerComponent> JobInstances = new Dictionary<String, ISchedulerComponent>();

        /// <summary>
        /// Notification handler service host
        /// </summary>
        /// <remarks>
        /// This si the actuall service host that controls the notification object <see>
        ///                                                                            <cref>ewsNotify</cref>
        ///                                                                        </see>
        /// </remarks>
        protected ServiceHost ServiceHost;

        #endregion

        #region Control functions

        public void Configure()
        {
            try
            {
                // NDC.Push("CONFIG");
                ThreadContext.Stacks["NDC"].Push("CONFIG");

                // Checking if a directory exists or not
                DirectoryInfo di = new DirectoryInfo(ConfigurationManager.AppSettings["OutPath"]);

                if (!di.Exists)
                {
                    di.Create();
                }

                // Read params 
                this.ConfigSet = ConfigurationManager.AppSettings["ConfigurationSet"];
                this.WatchConfigSet = Convert.ToBoolean(ConfigurationManager.AppSettings["WatchConfiguration"]);

                // Logging
                Logger.InfoFormat("{0} : {1}", "ConfigurationSet", this.ConfigSet);
                Logger.InfoFormat("{0} : {1}", "WatchConfiguration", this.WatchConfigSet);

                // Load configuration set
                XmlDocument config = new XmlDocument();
                config.Load(this.ConfigSet);

                // Setting up the watcher
                this.configFileWatcher.Path = Path.GetDirectoryName(this.ConfigSet);
                this.configFileWatcher.Filter = Path.GetFileName(this.ConfigSet);

                // Clearing all jobInstances before we populate them again. 
                this.JobInstances.Clear();

                // Creating Scheduler Components

                XmlNodeList nodes =
                    config.SelectNodes(
                        "/ComponentConfiguration/SchedulerComponents/SchedulerComponent[@Enabled='True']");
                if (nodes == null)
                {
                    throw new Exception("No jobs configured! Stopping service!");
                }
                Logger.InfoFormat("SchedulerComponent job instances found: {0}", nodes.Count);

                foreach (XmlNode nd in nodes)
                {
                    ISchedulerComponent schedulerComponent = new SchedulerComponent();

                    schedulerComponent.Configure(nd);

                    Logger.Debug("Adding " + schedulerComponent.InstanceName);
                    this.JobInstances.Add(schedulerComponent.InstanceName, schedulerComponent);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception);
                Mail.SendException(exception);
            }
            finally
            {
                // ThreadContext.Stacks["NDC"].Pop();
                NDC.Pop();
            }
        }

        /// <summary>
        /// Starts the main service instance.
        /// </summary>
        /// <remarks>
        /// The function will set up polling and events to Start all processing jobs.
        /// </remarks>
        public void Start()
        {
            NDC.Push("START");

            try
            {
                // Start instances
                Logger.Debug("Number of jobs to start: " + this.JobInstances.Count.ToString());

                // Looping jobs
                foreach (
                    KeyValuePair<string, ISchedulerComponent> kvp in this.JobInstances.Where(kvp => kvp.Value.Enabled))
                {
                    Logger.Info("Starting " + kvp.Value.InstanceName);
                    kvp.Value.Start();
                }

                // Starting maintenance
                Logger.Info("Starting maintenance timer");
                this.maintenanceTimer.Start();

                // Watch the config file
                this.configFileWatcher.EnableRaisingEvents = this.WatchConfigSet;
            }
            catch (Exception ex)
            {
                Logger.Fatal("An error has occured. Could not Start service", ex);
                Logger.Fatal(ex.StackTrace);
                Mail.SendException(ex);
            }
            finally
            {
                NDC.Pop();
            }
        }

        public void Pause()
        {
            this.maintenanceTimer.Stop();
            this.configFileWatcher.EnableRaisingEvents = false;

            // Stop instances
            foreach (KeyValuePair<string, ISchedulerComponent> kvp in this.JobInstances.Where(kvp => kvp.Value.Enabled))
            {
                Logger.Info("Stopping " + kvp.Value.InstanceName);
                kvp.Value.Stop();
            }
        }

        public void Stop()
        {
            this.Pause();

            // Kill notification service handler
            if (this.ServiceHost == null)
            {
                return;
            }
            this.ServiceHost.Close();
            this.ServiceHost = null;
        }

        #endregion
    }
}