﻿namespace DownloadScheduler.Components.Enums
{
    /// <summary>
    /// Specifies the different polling styles available to the EWS poller component
    /// </summary>
    public enum DownloadType
    {
        /// <summary>
        /// Indicates FTP download type
        /// </summary>
        Ftp,
        /// <summary>
        /// Indicates Http download type
        /// </summary>
        Http

    }
}